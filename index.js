/*
	Un mensaje tiene los paréntesis balanceados cuando cumple una de las siguientes reglas:
	1. Es un mensaje vacío
	2. Contiene sólo una o más repeticiones de las letras "a" a la "z", espacio " ", o dos puntos ":"
	3. Comienza por un paréntesis de apertura "(", seguido de un mensaje con paréntesis balanceados, seguido de un paréntesis de cierre ")"
	4. Es un mensaje con paréntesis balanceados seguido de otro mensaje con paréntesis balanceados
	5. Es un emoticón feliz ":)" o uno triste ":("

	Ejemplos:
	a. "hola" -> balanceado
	b. "(hola)" -> balanceado
	c. "(()" -> desbalanceado
	d. "(:)" -> balanceado (ej, si consideramos el mensaje como un ":" [regla 2] entre paréntesis [regla 3])
	e. "no voy (:()" -> balanceado (ej, si consideramos un emoticón triste [regla 5] entre paréntesis [regla 3])
	f. "hoy pm: fiesta :):)" -> balanceado
	g. ":((" -> desbalanceado
	h. "a (b (c (d) c) b) a :)" -> balanceado (ej, si el último paréntesis es en realidad un emoticón)
*/
const rules = [
	/(\(([a-z]|(\:\)|\:\(|\:)| )*\))+/gi,// Contenido interno en parentesis: a-z o :) o :( o : o espacio " "
	/\:\(/g, 							// Emoticón triste
	/\:\)/g, 							// Emoticón feliz
	/[a-z]/gi, 							// Todas las letras sin distincion de mayuscula/minuscula
	/\:/g, 								// Dos punto ":"
	/ /g								// Espacio en blanco
];

const balancedMessage = ( text, rule ) => {
	return text.replace( rule, '' );
};

const toBalance = ( e ) => {
	let value = e.target.value;
	document.getElementById( "text" ).innerHTML = value;

	if ( value.length > 0 ) {
		let selectRule = 0,
			compareText = value;

		/* Evaluar las reglas establecidas al valor recibido */
		while ( selectRule < rules.length ) {
			/* Se extra los mensajes balanceados entre parentesis */
			value = balancedMessage( value, rules[ selectRule ] );

			/* Si ya no se evalua la primera se sigue evaluando las siguietes */
			if ( selectRule ) selectRule++;

			/* Si ya no hay variación para la regla 1, se pasa a las siguiente regla*/
			value === compareText && !selectRule ? selectRule++ : compareText = value;

			/* Si no hay más qué evaluar, se ignoran el resto de las reglas */
			if ( value.length == 0 ) selectRule = 100;
		}
	}

	description.innerHTML = value.length ? 'desbalanceado' : 'balanceado';
};

/* Elemento donde se muestra el mensaje con el resultado de la evaluación */
const description = document.getElementById( "description" );
/* Elemento que contiene el mensaje */
const message = document.getElementById( "message" );
message.addEventListener( "keyup", toBalance, false );